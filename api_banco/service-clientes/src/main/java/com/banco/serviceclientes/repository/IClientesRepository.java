package com.banco.serviceclientes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banco.serviceclientes.entities.Cliente;


public interface IClientesRepository extends JpaRepository<Cliente,Integer> {
	
}
