package com.banco.serviceclientes.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cliente {
	private String tipoIdentificacion;
	@Id
	private int identificacion;
	private String nombre;
	private String apellido;
	private String email;
	private Date fechaNacimiento;
	private Date fechaCreacion;
	
	public Cliente () {
		this.tipoIdentificacion = "" ;
		this.identificacion = 0 ;
		this.nombre = "";
		this.apellido = "";
		this.email = "";
		this.fechaNacimiento = null;
		this.fechaCreacion = null;
	}
	public Cliente(String tipoIdentificacion, int identificacion, String nombre, String apellido, String email, Date fechaNacimiento, Date fechaCreacion) {
		this.tipoIdentificacion = tipoIdentificacion;
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaCreacion = fechaCreacion;
	}
	
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public int getIdentificacion() {
		return identificacion;
	}
	
	public void setIdentificacion(int identificacion) {
		this.identificacion = identificacion;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}	
}
