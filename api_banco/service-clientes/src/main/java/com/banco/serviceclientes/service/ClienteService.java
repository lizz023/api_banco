package com.banco.serviceclientes.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.serviceclientes.entities.Cliente;
import com.banco.serviceclientes.repository.IClientesRepository;

@Service
public class ClienteService {
	@Autowired
	private IClientesRepository clienteRepository;
	
	public List<Cliente> getClientes(){
		return clienteRepository.findAll();
	}
	
	public Cliente getCliente(int id) {
		Optional<Cliente> optionalCliente = clienteRepository.findById(id);
		Cliente cliente = optionalCliente.get();
		return cliente;
	}
	
	public void deleteCliente (int id) {
		clienteRepository.deleteById(id);
	}
	
	public String updateCliente (int id, Cliente clienteUpdated) {
		Cliente cliente = this.getCliente(id);
			cliente.setTipoIdentificacion(clienteUpdated.getTipoIdentificacion());
			cliente.setIdentificacion(clienteUpdated.getIdentificacion());
			cliente.setNombre(clienteUpdated.getNombre());
			cliente.setApellido(clienteUpdated.getApellido());
			cliente.setEmail(clienteUpdated.getEmail());
			cliente.setFechaNacimiento(clienteUpdated.getFechaNacimiento());
			cliente.setFechaCreacion(clienteUpdated.getFechaCreacion());
		this.createCliente(cliente);
		return "Cliente actualizado con éxito";
	}
	
	public String createCliente(Cliente cliente) {
		Cliente client = clienteRepository.save(cliente);
		return "Cliente guardado con éxito";
	}
	
	
}
