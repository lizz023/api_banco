package com.banco.serviceclientes.controllers;

import java.util.List;

import com.banco.serviceclientes.entities.Cliente;
import com.banco.serviceclientes.service.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ClientesCrontoller {
	
	@Autowired
	private ClienteService clienteService;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/clientes")
	public List<Cliente> getClientes() {
		List<Cliente> clientes = clienteService.getClientes();
		return clientes;
	}
	//Consultar informacion del clíente 
	@CrossOrigin(origins = "*")
	@GetMapping("/clientes/{id}")
	public Cliente getCliente(@PathVariable int id) {
		Cliente cliente = clienteService.getCliente(id);
		return cliente;
	}
	
	//Eliminar clientes
	@CrossOrigin(origins = "*")
	@DeleteMapping("/clientes/{id}")
	public void deleteCliente (@PathVariable int id) {
		clienteService.deleteCliente(id);
	}
	
	//Modificar clientes
	@CrossOrigin(origins = "*")
	@PutMapping("/clientes/{id}")
	public String updateCliente (@PathVariable int id, @RequestBody Cliente body) {
		String message = clienteService.updateCliente(id, body);
		return message;
	}
	
	//Crear un nuevo registro de cliente (Cliete nuevo)
	@CrossOrigin(origins = "*")
	@PostMapping("/clientes")
	public String createCliente (@RequestBody Cliente body) {
		Cliente cliente = new Cliente(
				body.getTipoIdentificacion(),
				body.getIdentificacion(),
				body.getNombre(),
				body.getApellido(),
				body.getEmail(),
				body.getFechaNacimiento(),
				body.getFechaCreacion()
				);
		String message = clienteService.createCliente(cliente);
		return message;		
	}
	
	
}