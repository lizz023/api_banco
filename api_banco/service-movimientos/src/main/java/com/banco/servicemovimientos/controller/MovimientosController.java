package com.banco.servicemovimientos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.banco.servicemovimientos.entities.Movimiento;
import com.banco.servicemovimientos.models.MovimientoRequest;
import com.banco.servicemovimientos.service.MovimientoService;


@RestController
public class MovimientosController {
	
	@Autowired
	private MovimientoService ms;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/movimientos")
	public List<Movimiento> movimiento (){
		List<Movimiento> movimientos = ms.getMovimientos();
		return movimientos;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/movimientos/agregar")
	public String agegarTransaccion(@RequestBody MovimientoRequest body) {
		String respuesta = ms.addMovimiento(body);
		return respuesta;
	}
	

}
