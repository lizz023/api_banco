package com.banco.servicemovimientos.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Producto {
	private String tipo;
	@Id
	private int numero;
	private Date fechaApertura;
	private String estado;
	private int saldo;
	private int idCliente;
	
	
	public Producto() {
		this.tipo = "" ;
		this.numero = 0 ;
		this.fechaApertura =null;
		this.estado = "";
		this.saldo = 0;
		this.idCliente= 0;
	}
	
	public Producto(String tipo, int numero, Date fechaApertura, String estado, int saldo, int idCliente){
		this.tipo = tipo;
		this.numero = numero;
		this.fechaApertura = fechaApertura;
		this.estado = estado;
		this.saldo = saldo;
		this.idCliente = idCliente;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public Date getFechaApertura() {
		return fechaApertura;
	}
	
	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public int getSaldo() {
		return saldo;
	}
	
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	
	public int getIdCliente() {
		return idCliente;
	}
	
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
}
