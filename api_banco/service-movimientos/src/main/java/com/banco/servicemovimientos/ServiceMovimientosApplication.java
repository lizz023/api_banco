package com.banco.servicemovimientos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceMovimientosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceMovimientosApplication.class, args);
	}

}
