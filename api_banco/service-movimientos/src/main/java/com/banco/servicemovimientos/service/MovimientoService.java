package com.banco.servicemovimientos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.banco.servicemovimientos.entities.Movimiento;
import com.banco.servicemovimientos.entities.Producto;
import com.banco.servicemovimientos.models.MovimientoRequest;
import com.banco.servicemovimientos.repository.IMovimientoRepository;


@Service
public class MovimientoService {
	@Autowired
	private IMovimientoRepository movimientoRepository;
	private final RestTemplate restTemplate;
	
	public MovimientoService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate= restTemplateBuilder.build();
	}
	
	private int getGMF(int monto) {
		return monto*4/1000;
	}
	
	private boolean cumpleMontoMinimo(Producto producto, int monto) {
		int gmf = this.getGMF(monto);
		return 
			(producto.getTipo().equals("AHORROS") && producto.getSaldo() >= monto + gmf) // Restricción monto mínimo para cuentas de ahorros = 0
		|| (producto.getTipo().equals("CORRIENTE") && producto.getSaldo() >= monto + gmf - 2000000); // Restricción monto mínimo cuenta corriente = -2'000.000
	}
	
	public Producto getProductoById(int numeroCuenta) {
		return this.restTemplate.getForObject("http://localhost:9191/productos/" +  numeroCuenta, Producto.class);
	}
	
	public String saveProducto(Producto producto){
		String url = "http://localhost:9191/productos";
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Producto> entity = new HttpEntity<>(producto, headers);
		return this.restTemplate.postForObject(url,entity,String.class);
	}
	
	public List<Movimiento> getMovimientos(){
		return movimientoRepository.findAll();
	}
	
	public String addMovimiento(MovimientoRequest movimiento) {
		Producto productoDestino;
		Producto productoOrigen;
		int saldoActual;
		int gmf;
		
		switch(movimiento.getTipoTransaccion()) {
			case "CONSIGNACION":
				try {
					productoDestino = getProductoById(movimiento.getCuentaDestino());
				} catch (Exception e1) {
					return "Producto destino no existe";
				}
				if(!productoDestino.getEstado().equals("CANCELADO")) {
					saldoActual= productoDestino.getSaldo();
					productoDestino.setSaldo(saldoActual + movimiento.getValor());
					this.saveProducto(productoDestino);
					return "Consignación realizada con exito";
				}else {
					return "No se puede consignar a cuentas canceladas";
				}
			
			case "RETIRO":
				try {
					productoOrigen = getProductoById(movimiento.getCuentaOrigen());
				} catch (Exception e) {
					return "Producto de origen no existe";
				}
				if(productoOrigen.getEstado().equals("ACTIVO")) {
					saldoActual = productoOrigen.getSaldo();
					gmf = this.getGMF(movimiento.getValor());
					
					if(this.cumpleMontoMinimo(productoOrigen, movimiento.getValor())) {
						productoOrigen.setSaldo(saldoActual- movimiento.getValor() - gmf);
						this.saveProducto(productoOrigen);
						return "Retiro realizado con exito";
					}else {
						return "No se pudo realizar el retiro: Fondos insuficientes";
					}
				}else {
					return "Las cuentas NO ACTIVAS no admiten retiros"; 
				}
				
			case "TRANSFERENCIA":
				try {
					productoOrigen = getProductoById(movimiento.getCuentaOrigen());
				} catch (Exception e) {
					return "Producto origen no existe";
				}				
				
				if(productoOrigen.getEstado().equals("ACTIVO")) {
					saldoActual = productoOrigen.getSaldo();
					gmf = this.getGMF(movimiento.getValor());
					
					if(this.cumpleMontoMinimo(productoOrigen, movimiento.getValor())){						
						try {
							productoDestino = getProductoById(movimiento.getCuentaDestino());
						} catch (Exception e) {
							return "Producto destino no existe";
						}
						
						if(productoDestino.getEstado().equals("ACTIVO")) {
							productoOrigen.setSaldo(saldoActual- movimiento.getValor() - gmf);
							this.saveProducto(productoOrigen);
							
							saldoActual= productoDestino.getSaldo();
							productoDestino.setSaldo(saldoActual + movimiento.getValor());
							this.saveProducto(productoDestino);
							
						}else {
							return "No se pudo realizar la transferencia: Cuenta destino no esta activa";
						}
						
					}else {
						return "No se pudo realizar la transferencia. Fondos insuficientes en cuenta origen";
					}
				}else {
					return "Las cuentas NO ACTIVAS no admiten transferencias"; 
				}
			default:
				return "Tipo de transaccion no valido";
				
		}
	}
}
