package com.banco.servicemovimientos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banco.servicemovimientos.entities.Movimiento;


public interface IMovimientoRepository extends JpaRepository<Movimiento,Integer>{
}
