package com.banco.servicemovimientos.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.lang.Nullable;

@Entity
public class Movimiento {
	@Id
	private int numeroTransaccion;
	private String tipoTransaccion;
	private Date fecha;
	private String tipoMovimiento;
	private String descripcion;
	private int valor;
	private int saldo;
	private double gmf;
	private int cuentaOrigen;
	private int cuentaDestino;
	
	public Movimiento () {
		this.numeroTransaccion= 0;
		this.tipoTransaccion = "" ;
		this.fecha = null ;
		this.tipoMovimiento ="";
		this.descripcion = "";
		this.valor = 0;
		this.saldo= 0;
		this.gmf= 0;
		this.cuentaOrigen= 0;
		this.cuentaDestino= 0;
	}
	
	public Movimiento(String tipoTransaccion, Date fecha, String tipoMovimiento, String descripcion, int valor, int saldo, double gmf, int cuentaOrigen, int cuentaDestino, int numeroTransaccion){
		this.numeroTransaccion = numeroTransaccion;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = fecha;
		this.tipoMovimiento = tipoMovimiento;
		this.descripcion = descripcion;
		this.valor = valor;
		this.saldo = saldo;
		this.gmf= gmf;
		this.cuentaOrigen = cuentaOrigen;
		this.cuentaDestino = cuentaDestino;
	}
	
	public int getNumeroTransaccion() {
		return numeroTransaccion;
	}

	public void setNumeroTransaccion(int numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}
	
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}
	
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public double getGmf() {
		return gmf;
	}

	public void setGmf(double gmf) {
		this.gmf = gmf;
	}

	public int getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(int cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public int getCuentaDestino() {
		return cuentaDestino;
	}

	public void setCuentaDestino(int cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	
	
	
}
