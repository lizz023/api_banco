package com.banco.serviceproductos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.banco.serviceproductos.entities.Producto;
import com.banco.serviceproductos.service.ProductoService;


@RestController
public class ProductosController {
	
	@Autowired
	private ProductoService productoService;
	
	//Trae los productos ya existentes
	@CrossOrigin(origins = "*")
	@GetMapping("/productos")
	public List<Producto> getProductos(){
		List<Producto> productos = productoService.getProductos();
		return productos;
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/productos/{id}")
	public Producto getProducto(@PathVariable int id) {
		Producto producto = productoService.getProducto(id);
		return producto;
	}
	//Cancelar cuentas
	//@PatchMapping("productos/updateStatus/{id}")
	//public String cancelCuenta(@PathVariable int id, @RequestBody  String body){
		//String respuesta = productoService.updateStatusProducto(id, body.newStatus);
		//return respuesta;
	//}
	
	//Crear o actualizar un registro de producto
	@CrossOrigin(origins = "*")
	@PostMapping("/productos")
	public String saveProducto (@RequestBody Producto body) {
		Producto producto = new Producto(
			body.getTipo(),
			body.getNumero(),
			body.getFechaApertura(),
			body.getEstado(),
			body.getSaldo(),
			body.getIdCliente()
		);
		String message = productoService.saveProducto(producto);
		return message;
	}	
}
