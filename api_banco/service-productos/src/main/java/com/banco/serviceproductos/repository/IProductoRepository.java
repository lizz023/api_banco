package com.banco.serviceproductos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banco.serviceproductos.entities.Producto;

public interface IProductoRepository extends JpaRepository<Producto,Integer> {

}
