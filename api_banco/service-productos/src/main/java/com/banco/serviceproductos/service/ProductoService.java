package com.banco.serviceproductos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.serviceproductos.entities.Producto;
import com.banco.serviceproductos.repository.IProductoRepository;

@Service
public class ProductoService {

	@Autowired
	private IProductoRepository productoRepository;
	
	public List<Producto> getProductos(){
		return productoRepository.findAll();
	}
	
	public Producto getProducto(int id) {
		Optional<Producto> optionalProducto = productoRepository.findById(id);
		Producto producto = optionalProducto.get();
		return producto;
	}
	
	public String saveProducto(Producto producto) {
		try {
			Producto product = productoRepository.save(producto);
			return "El producto ha sido guardado";
		} catch(Exception e) {
			return "El producto no pudo ser guardado: " + e.getMessage(); 
		}		
	}
	
	
	public String updateStatusProducto (int id, String newEstado){
		Producto producto =  this.getProducto(id);
		if (newEstado == "CANCELADO") {
			if (producto.getSaldo()==0) {
				producto.setEstado(newEstado);
				this.saveProducto(producto);
				return "Producto Cancelado";
			}else {
				return "El producto no pudo ser cancelado";
			}
		}else {
			producto.setEstado(newEstado);
			return "El estado del producto fue cambiado a: " + newEstado;
		}
	}
	
}
