import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Cliente } from './cliente.model';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  clientesList = new Subject <Cliente[]>();
  public clientes: Cliente[];

  constructor(private http:HttpClient) { }//Permite mandar peticiones get,post,patch,put

  getClientes(){
    this.http.get(environment.apiUrl + environment.apiPort + "/clientes")
      .subscribe((clients:Cliente[]) => {
        this.clientes= clients;
        this.clientesList.next(this.clientes);
      });
  }

  getCliente(id:number): Cliente{
    return this.clientes.find(cliente => cliente.identificacion == id);
  }

  updateCliente(cliente:Cliente){
    return this.http.put(environment.apiUrl + environment.apiPort + "/clientes/" + cliente.identificacion, cliente)
    .subscribe(() => console.log("hola"));
  }
}
