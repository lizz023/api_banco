export class Cliente {
    public tipoIdentificacion: String;
	public identificacion: number;
	public nombre: String;
	public apellido: String;
	public email: String;
	public fechaNacimiento: Date;
	public fechaCreacion: Date;

    constructor(tipoIdentificacion: String,identificacion: number,nombre: String,apellido: String,email: String,fechaNacimiento: Date,fechaCreacion: Date){
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaCreacion = fechaCreacion;
    }
}