import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Cliente } from '../cliente.model';
import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css']
})
export class ClienteFormComponent implements OnInit {
  @ViewChild("updateClienteForm",{static: false}) ucForm: NgForm
  private id;
  public cliente: Cliente; 

  constructor(private route:ActivatedRoute, private clienteService:ClienteService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:Params) => {
      this.id = +params['id'];
      this.cliente = this.clienteService.getCliente(this.id);
    })
  }

  updateCliente(form: NgForm){
    const value = form.value;
    console.log(this.cliente);
    console.log(value);
    this.clienteService.updateCliente(this.cliente);
  }

}
