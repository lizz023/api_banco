import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente.model';
import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {//Hook q se ejecuta cuando se crea el componente (Se crea cuando se llama)
  clientes: Cliente[] =[]
  constructor(private clienteService:ClienteService) { }

  ngOnInit() {//metodo que exige la interfaz onInit
    this.clienteService.clientesList.subscribe((clients:Cliente[]) => {
      this.clientes= clients;
    })
    this.clienteService.getClientes();
  }
}
